/*
 Publishing in the callback

  - connects to an MQTT server
  - subscribes to the topic "inTopic"
  - when a message is received, republishes it to "outTopic"

  This example shows how to publish messages within the
  callback function. The callback function header needs to
  be declared before the PubSubClient constructor and the
  actual callback defined afterwards.
  This ensures the client reference in the callback function
  is valid.

*/

#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.
byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };
IPAddress ip(192, 168, 178, 234);
IPAddress server(192, 168, 178, 11);

byte devId = 49; // (in DEC notation 1 = 49 )
int output[] = { 2, 3, 4, 5, 6, 7, 8, 9};
int outputLen = 8;
// outputs in DEC notation 1 = 49:
byte outputBytes[] = { 50, 51, 52, 53, 54, 55, 56, 57 };


// find var type 
// Generic catch-all implementation.
template <typename T_ty> struct TypeInfo { static const char * name; };
template <typename T_ty> const char * TypeInfo<T_ty>::name = "unknown";

// Handy macro to make querying stuff easier.
#define TYPE_NAME(var) TypeInfo< typeof(var) >::name

// Handy macro to make defining stuff easier.
#define MAKE_TYPE_INFO(type)  template <> const char * TypeInfo<type>::name = #type;

// Type-specific implementations.
MAKE_TYPE_INFO( int )
MAKE_TYPE_INFO( float )
MAKE_TYPE_INFO( short )
MAKE_TYPE_INFO( char )
MAKE_TYPE_INFO( byte )

// Callback function header
void callback(char* topic, byte* payload, unsigned int length);

EthernetClient ethClient;
PubSubClient client(server, 1883, callback, ethClient);

// Callback function
void callback(char* topic, byte* payload, unsigned int length) {
  // In order to republish this payload, a copy must be made
  // as the orignal payload buffer will be overwritten whilst
  // constructing the PUBLISH packet.

  // Allocate the correct amount of memory for the payload copy
  byte* p = (byte*)malloc(length);
  // Copy the payload to the new buffer
  memcpy(p,payload,length);

  //  payload comming in:
  //  1:6:1 
  //  +-|-|---devId
  //    +-|---output
  //      +---state 1=on 0=off
  
  int success = 0;                                // broken is default ;-)
  
  if(payload[0] == devId){                        // compare first char to devId (request for me or someone else)
    int i; for (i = 0; i < outputLen; i++ ) {     // loop over the outputs to see if we have the requested output
      if(payload[2] == outputBytes[i] ){          // output in request matches current output in loop
        int led = output[i];                      // set led pin to selected output
        if(payload[4] == '0'){                    // requested off signal
          digitalWrite(led, LOW);                 // turn off the led
          success = 1;                            // mark transaction successful
        } else if (payload[4] == '1'){            // requested on signal
          digitalWrite(led, HIGH);                // turn on the led
          success = 1;                            // mark transaction successful
        }
      }
    }
  }

  // log and callback
  if (success == 1){
    client.publish("iot/ack", p, length);        // callback to ack the request
  } else {
    client.publish("iot/ack", "ERROR: something went wrong processing this request", length); // callback to show error
    Serial.println("ERROR: something went wrong processing this request");
  }
  
  // Free the memory
  free(p);
}

void setup()
{
  Serial.begin(57600);

  // Set output on the output pins
  int i; for (i = 0; i < outputLen; i++ ) {;
    pinMode(output[i], OUTPUT);
  }

  Ethernet.begin(mac, ip);
  if (client.connect("arduinoClient")) {
    client.publish("iot/ack", "1:5-8");
    client.subscribe("iot/req");
  }
}

void loop()
{
  client.loop();
}
